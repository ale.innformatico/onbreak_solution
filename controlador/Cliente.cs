﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos
{
    class Cliente
    {
        private string rut;
        private string razonSocial;
        private string nombreContacto;
        private string mailContacto;
        private string direccion;
        private int telefono;

        private enum actividadEmpresa
        {
            Agropecuaria,
            Mineria,
            Manufactura,
            Comercio,
            Hoteleria,
            Alimentos,
            Transporte,
            Servicios
        };
        private enum tipoEmpresa
        {
            SPA,
            EIRL,
            LIMITADA,
            SOCIEDAD_ANONIMA
        };

        public Cliente()
        {
        }

        public Cliente(string rut, string razonSocial, string nombreContacto, string mailContacto, string direccion, int telefono)
        {
            this.Rut = rut;
            this.RazonSocial = razonSocial;
            this.NombreContacto = nombreContacto;
            this.MailContacto = mailContacto;
            this.Direccion = direccion;
            this.Telefono = telefono;
        }

        public string Rut { get => rut; set => rut = value; }
        public string RazonSocial { get => razonSocial; set => razonSocial = value; }
        public string NombreContacto { get => nombreContacto; set => nombreContacto = value; }
        public string MailContacto { get => mailContacto; set => mailContacto = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public int Telefono { get => telefono; set => telefono = value; }

    }
}
