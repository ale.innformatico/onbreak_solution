﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos
{
    class TipoEvento
    {
        private int id;
        private string nombre;
        private double valorBase;
        private int personalBase;

        public TipoEvento()
        {
        }

        public TipoEvento(int id, string nombre, double valorBase, int personalBase)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.ValorBase = valorBase;
            this.PersonalBase = personalBase;
        }

        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public double ValorBase { get => valorBase; set => valorBase = value; }
        public int PersonalBase { get => personalBase; set => personalBase = value; }
    }
}
