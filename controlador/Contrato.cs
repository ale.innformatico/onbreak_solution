﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDeDatos
{
    class Contrato
    {
        private int numeroContrato;
        private DateTime fechaCreacion;
        private DateTime fechaTermino;
        private DateTime fechaHoraInicio;
        private DateTime fechaHoraTermino;
        private string direccion;
        private Boolean estaVigente;
        private string observaciones;
    
        private enum tipoEvento
        {
            FIESTA_CORPORATIVA,
            CUMPLEANIOS,
            ANIVERSARIO,
            FIESTA_ANIO_NUEVO
        };

        private enum tipoContrato
        {
            ANUAL,
            SEMESTRAL,
            POR_EVENTO
        };



        public Contrato(int numeroContrato, DateTime fechaCreacion, DateTime fechaTermino, 
            DateTime fechaHoraInicio, DateTime fechaHoraTermino, string direccion, 
            bool estaVigente, string observaciones)
        {
            this.NumeroContrato = numeroContrato;
            this.FechaCreacion = fechaCreacion;
            this.FechaTermino = fechaTermino;
            this.FechaHoraInicio = fechaHoraInicio;
            this.FechaHoraTermino = fechaHoraTermino;
            this.Direccion = direccion;
            this.EstaVigente = estaVigente;
            this.Observaciones = observaciones;
        }

        public int NumeroContrato { get => numeroContrato; set => numeroContrato = value; }
        public DateTime FechaCreacion { get => fechaCreacion; set => fechaCreacion = value; }
        public DateTime FechaTermino { get => fechaTermino; set => fechaTermino = value; }
        public DateTime FechaHoraInicio { get => fechaHoraInicio; set => fechaHoraInicio = value; }
        public DateTime FechaHoraTermino { get => fechaHoraTermino; set => fechaHoraTermino = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public bool EstaVigente { get => estaVigente; set => estaVigente = value; }
        public string Observaciones { get => observaciones; set => observaciones = value; }
    }
}
